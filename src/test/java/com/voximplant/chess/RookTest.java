package com.voximplant.chess;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class RookTest {
	@Test
	public void deadEndTest() {
		List<PieceFactory> pieceFactories = Arrays.asList(
				chessboard -> new Rook(chessboard, new Location(0, 0)),
				chessboard -> new Rook(chessboard, new Location(4, 0))
		);
		Chessboard chessboard = new Chessboard(pieceFactories);
		Rook rook = (Rook) chessboard.getPieces().get(0);
		assertFalse(rook.canMove(new Location(5, 0)));
	}

	@Test
	public void clearWayTest() {
		List<PieceFactory> pieceFactories = Collections.singletonList(
				chessboard -> new Rook(chessboard, new Location(0, 0))
		);
		Chessboard chessboard = new Chessboard(pieceFactories);
		Rook rook = (Rook) chessboard.getPieces().get(0);
		assertTrue(rook.canMove(new Location(2, 0)));
	}

	@Test
	public void incorrectWayTest() {
		List<PieceFactory> pieceFactories = Collections.singletonList(
				chessboard -> new Rook(chessboard, new Location(0, 0))
		);
		Chessboard chessboard = new Chessboard(pieceFactories);
		Rook rook = (Rook) chessboard.getPieces().get(0);
		assertFalse(rook.canMove(new Location(2, 2)));
	}
}
