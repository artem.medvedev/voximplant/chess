package com.voximplant.chess;

public final class Rook extends Piece {

	public Rook(Chessboard chessboard, Location location) {
		super(chessboard, location);
	}

	@Override
	protected boolean canMove(Location location) {
		if (location == null) throw new IllegalArgumentException("Location is null.");
		Location current = currentLocation;
		if (current == null) throw new IllegalStateException("Current location is null.");
		if (current.equals(location)) return false;
		if (current.getX() != location.getX() && current.getY() != location.getY()) {
			return false;
		}
		if (current.getX() == location.getX()) {
			int increment = current.getY() < location.getY() ? 1 : -1;
			int temp = current.getY();
			do {
				temp += increment;
				if (chessboard.hasPiece(new Location(location.getX(), temp))) {
					return false;
				}
			}
			while (temp != location.getY());
			return true;
		} else {
			int increment = current.getX() < location.getX() ? 1 : -1;
			int temp = current.getX();
			do {
				temp += increment;
				if (chessboard.hasPiece(new Location(temp, location.getY()))) {
					return false;
				}
			}
			while (temp != location.getX());
			return true;
		}
	}
}
