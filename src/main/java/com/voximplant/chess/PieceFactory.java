package com.voximplant.chess;

@FunctionalInterface
public interface PieceFactory {
	Piece createPiece(Chessboard chessboard);
}
