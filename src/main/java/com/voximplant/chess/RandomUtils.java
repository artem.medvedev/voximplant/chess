package com.voximplant.chess;

import java.util.Random;

public class RandomUtils {
	private static final Random random = new Random();

	public static int generateInt(int min, int max) {
		return random.nextInt((max - min) + 1) + min;
	}

	public static Location generateLocation() {
		int x = generateInt(0, Chessboard.ROWS - 1);
		int y = generateInt(0, Chessboard.COLUMNS - 1);
		return new Location(x, y);
	}

	private static int generateCoordinate(int currentCoordinate) {
		int coordinate;
		do {
			coordinate = generateInt(0, Chessboard.COLUMNS - 1);
		}
		while (coordinate == currentCoordinate);
		return coordinate;
	}

	public static Location generateCastleLocation(Location currentLocation) {
		if (random.nextBoolean()) {
			return new Location(currentLocation.getX(), generateCoordinate(currentLocation.getY()));
		} else {
			return new Location(generateCoordinate(currentLocation.getX()), currentLocation.getY());
		}
	}
}
