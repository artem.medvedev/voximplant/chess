package com.voximplant.chess;

public abstract class Piece extends Thread {

	private static final int NEEDED_MOVES_COUNT = 50;
	private static final int WAITING_LIMIT = 5000;

	protected final Chessboard chessboard;
	protected volatile Location currentLocation;
	protected volatile int movesCount = 0;

	public Piece(Chessboard chessboard, Location location) {
		if (chessboard == null) throw new IllegalArgumentException("Chessboard is null.");
		if (location == null) throw new IllegalArgumentException("Chessboard is null.");
		this.chessboard = chessboard;
		this.currentLocation = location;
		setName("Castle thread");
	}

	public Location getCurrentLocation() {
		return currentLocation;
	}

	protected abstract boolean canMove(Location location);

	@Override
	public void run() {
		super.run();
		while (movesCount < NEEDED_MOVES_COUNT) {
			try {
				Location location = RandomUtils.generateCastleLocation(currentLocation);
				long start = System.currentTimeMillis();
				while (true) {
					synchronized (chessboard.getSyncRoot()) {
						if (canMove(location)) {
							moveTo(location);
							movesCount++;
							if (movesCount >= NEEDED_MOVES_COUNT) {
								chessboard.removeFromBoard(this);
								System.out.println(format("job is over."));
							}
							break;
						}
					}
					if (System.currentTimeMillis() - start > WAITING_LIMIT) {
						System.out.println(format("waiting limit is expired"));
						break;
					}
					Thread.sleep(100);
				}
				Thread.sleep(300);
			} catch (InterruptedException exc) {
				System.out.println(format("thread sleep was interrupted."));
			}
		}
	}

	protected String format(String message) {
		return String.format("Thread id %d, name %s, moves %d, location %s: %s", getId(), getName(), movesCount, currentLocation, message);
	}

	public void moveTo(Location location) {
		System.out.println(format("moved to " + location));
		chessboard.updatePosition(this, currentLocation, location);
		currentLocation = location;
	}

	@Override
	public String toString() {
		return this.getClass().getName() + "{" +
				"currentLocation=" + currentLocation +
				'}';
	}
}
