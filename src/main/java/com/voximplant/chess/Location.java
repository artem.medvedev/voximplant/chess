package com.voximplant.chess;

import java.util.Objects;

public final class Location {
	private final int x;
	private final int y;

	public Location(int x, int y) {
		if (x < 0 || x > Chessboard.ROWS) throw new IllegalArgumentException("x is out of range.");
		if (y < 0 || y > Chessboard.COLUMNS) throw new IllegalArgumentException("y is out of range.");

		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Location location = (Location) o;
		return x == location.x &&
				y == location.y;
	}

	@Override
	public int hashCode() {
		return Objects.hash(x, y);
	}

	@Override
	public String toString() {
		return "Location{" +
				"x=" + x +
				", y=" + y +
				'}';
	}
}
