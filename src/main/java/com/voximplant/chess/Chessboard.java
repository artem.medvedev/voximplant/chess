package com.voximplant.chess;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Chessboard {
	public static final int ROWS = 8;
	public static final int COLUMNS = 8;
	public static final int SQUARES = ROWS * COLUMNS;

	private final Object syncRoot;
	private final Piece[][] board;
	private final List<Piece> pieces;

	public Chessboard(List<PieceFactory> pieceFactories) {
		if (pieceFactories == null) throw new IllegalArgumentException("Pieces is null.");
		if (pieceFactories.size() > SQUARES) {
			throw new IllegalArgumentException("Pieces count less than zero or more than squares on the chessboard");
		}
		this.syncRoot = new Object();
		this.board = new Piece[ROWS][COLUMNS];
		this.pieces = setUpBoard(pieceFactories);
	}

	public Chessboard(int piecesCount) {
		if (piecesCount <= 0 || piecesCount > SQUARES) {
			throw new IllegalArgumentException("Pieces count less than zero or more than squares on the chessboard");
		}
		this.syncRoot = new Object();
		this.board = new Piece[ROWS][COLUMNS];
		this.pieces = new ArrayList<>(piecesCount);

		fillBoard(piecesCount);
	}

	Object getSyncRoot() {
		return syncRoot;
	}

	public List<Piece> getPieces() {
		return pieces;
	}

	public void startChaos() {
		for (Piece piece : pieces) {
			piece.start();
		}
	}

	private List<Piece> setUpBoard(List<PieceFactory> pieceFactories) {
		List<Piece> pieces = new ArrayList<>(pieceFactories.size());
		for (PieceFactory pieceFactory : pieceFactories) {
			Piece piece = pieceFactory.createPiece(this);
			Location location = piece.getCurrentLocation();
			board[location.getX()][location.getY()] = piece;
			pieces.add(piece);
		}
		return pieces;
	}

	private void fillBoard(int piecesCount) {
		Object dummy = new Object();
		HashMap<Location, Object> generatedPieces = new HashMap<>(piecesCount);
		while (generatedPieces.size() != piecesCount) {
			Location location = RandomUtils.generateLocation();
			if (generatedPieces.putIfAbsent(location, dummy) == null) {
				Piece piece = new Rook(this, location);
				board[location.getX()][location.getY()] = piece;
				pieces.add(piece);
			}
		}
	}

	boolean hasPiece(Location location) {
		if (location == null) throw new IllegalArgumentException("Chessboard is null.");
		return board[location.getX()][location.getY()] != null;
	}

	void updatePosition(Piece piece, Location oldLocation, Location newLocation) {
		board[oldLocation.getX()][oldLocation.getY()] = null;
		board[newLocation.getX()][newLocation.getY()] = piece;
	}

	void removeFromBoard(Piece piece) {
		pieces.remove(piece);
		Location location = piece.getCurrentLocation();
		board[location.getY()][location.getY()] = null;
	}
}
